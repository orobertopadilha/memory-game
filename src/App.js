import './App.css';
import React from 'react';
import ColorBlock from './ui/color-block/ColorBlock';
import ColorBlockList from './ui/color-list/ColorBlockList';

const allColors = [
    "#e53935", "#d81b60", "#8e24aa", "#5e35b1",
    "#3949ab", "#1e88e5", "#039be5", "#00acc1",
    "#00897b", "#43a047", "#7cb342", "#c0ca33",
    "#fdd835", "#ffb300", "#fb8c00", "#f4511e",
    "#6d4c41", "#757575", "#546e7a", "#000000"
]

class App extends React.Component {

    constructor() {
        super() 
        this.state = {
            colors: [],
            answers: ['', '', '', '', ''],
            colorIndex: 0,
            memorizing: true,
            currentAnswer: 0
        }
    }

    componentDidMount() {
        this.drawColors()
    }

    drawColors() {
        let tempColors = []
        let lastColor = ''

        for (let i = 0; i < 5; i++) {
            let currentColor = ''

            do {
                let pos = Math.floor(Math.random() * allColors.length)
                currentColor = allColors[pos] 
            } while (lastColor === currentColor)

            tempColors.push(currentColor)
            lastColor = currentColor
        }
        console.log(tempColors)
        this.setState({ colors: tempColors })
        setTimeout( () => this.showNextColor(), 1500)
    }

    showNextColor() {
        let nextIndex = this.state.colorIndex + 1
        
        if (nextIndex < 5) {
            this.setState({
                colorIndex: nextIndex
            })
            setTimeout( () => this.showNextColor(), 1500) 
        } else {
            this.setState({ 
                memorizing: false
            })
        }
    }

    fillAnswer(color) {
        let currentAnswer = this.state.currentAnswer
        
        if (currentAnswer < 5) {
            let newAnswers = this.state.answers
            newAnswers[currentAnswer] = color
            
            this.setState({
                currentAnswer: currentAnswer + 1,
                answers: newAnswers
            })
        }
    }

    validateAnswers() {
        let isCorrect = this.state.colors.every( (color, index) => { 
            return color === this.state.answers[index]
        })

        if (isCorrect) {
            alert('Você acertou!')
        } else {
            alert('Você errou!')
        }
    }

    render() {
        return (
            <>
                {
                    this.state.memorizing ? 
                    <div>
                        Memorize a sequência de cores: <br />
                        <ColorBlock color={this.state.colors[this.state.colorIndex]} />
                    </div>
                    :
                    <div>
                        <ColorBlockList colors={this.state.answers} />

                        <p className="post-wrapper">
                            Clique nas cores para preencher a sequência:
                        </p>
                        
                        <ColorBlockList colors={allColors} onItemClick={(color) => this.fillAnswer(color)} />

                        <div className="post-wrapper">
                            <input type="button" value="Validar!" 
                                disabled={this.state.currentAnswer < 5}
                                onClick={() => this.validateAnswers()}/>
                        </div>
                    </div>
                }               
            </>
        )
    }
}

export default App;
