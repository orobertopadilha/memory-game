import ColorBlock from '../color-block/ColorBlock'
import './ColorBlockList.css'

const ColorBlockList = ({ colors, onItemClick }) => {
    
    return (
        <div className="box-wrapper">
            { 
                colors.map((color, index) => 
                    <ColorBlock key={index} size="medium" color={color} onClick={() => onItemClick(color)} />
                ) 
            }
        </div>
    )
}

export default ColorBlockList