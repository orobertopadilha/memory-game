import "./ColorBlock.css"

const ColorBlock = ({color, size, onClick}) => {

    return (
        <div className={ size === 'medium' ? 'box-medium' : 'box' } 
             style={ { backgroundColor: color } } onClick={onClick}></div>
    )
}

export default ColorBlock